# Gaia Sky data descriptor

This small repository contains the data descriptor file for Gaia Sky.
This file is served by a mirror repository and downloaded at startup by Gaia Sky. It defines the catalog and data packs offered by the repository, which Gaia Sky compares to the local (previously downloaded) ones.

Each data pack definition contains a key, a name, a version, a description, some release notes, a link, the type of dataset, the (compressed) size, the number of objects it contains, the main file (usually `catalog-[name].json`) and a listing of all files in the pack, possibly using wildcards. It also has the `sha256` checksum and the location of the dataset file. Here is an example for the DR3-small dataset.

```json
    {
      "key": "gaia-dr3-small",
      "name": "Gaia DR3 small",
      "version": 0,
      "type": "catalog-lod",
      "mingsversion": 30200,
      "description": "Star catalog based on Gaia DR3 with a small number of stars. Contains all stars with up to 10%/0.5% bright/faint parallax relative error.",
      "releasenotes": "- Contains a selection of DR3 and all Hipparcos stars.\n- The cross-match table used is in the archive.\n- Distances are derived from parallaxes (d=1000/plx).\n- Parallaxes are using the corrected terms.\n- Extinction and reddening corrections are applied to magnitudes and colors.",
      "link": "@mirror-url@catalog/dr3/001-small",
      "size": 560062416,
      "nobjects": 8199560,
      "check": "catalog-gaia-dr3-small.json",
      "data": [
        "catalog-gaia-dr3-small.json",
        "catalog/gaia-dr3-small"
      ],
      "sha256": "10754d1e4bfe0ab604b8dc2cddd6776ad76bfea2890a36716ea9c347cf2dcd3c",
      "file": "@mirror-url@catalog/dr3/001-small/v00_20220608/catalog-gaia-dr3-small.tar.gz"
    }
```


- [Gaia Sky](https://codeberg.org/gaiasky/gaiasky)
- [Project documentation](https://gaia.ari.uni-heidelberg.de/gaiasky/docs)
- [Data repository](https://gaia.ari.uni-heidelberg.de/gaiasky/repository)
- [Releases repository](https://gaia.ari.uni-heidelberg.de/gaiasky/releases)
